#include<stdio.h>

int fibonacciseq(int n);

int fibonacciseq(int n) {
	if (n==0) {
		return 0;
	}
	if (n==1) {
		return 1;
	}
	return fibonacciseq(n-1) + fibonacciseq(n-2);
}

int main() {
	int i,n;
	printf("enter lines: ");
	scanf("%d",&i);
	for (n=0;n<=i;n++) {
		printf("%d\n",fibonacciseq(n));
	}
	return 0;
}
